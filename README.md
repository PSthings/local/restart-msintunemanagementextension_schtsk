# Restart-MSIntuneManagementExtension_SchTsk


Registers a scheduled task which Triggers a onetime restart of service IntuneManagementExtension. Forces device to sync with Intune Management
Runs at any user logon and deletes itself after 60 mins
