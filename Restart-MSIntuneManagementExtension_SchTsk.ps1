#------------------------------------------------------------#
# Registers a scheduled task which Triggers a onetime restart of service IntuneManagementExtension. Forces device to sync with Intune Management
# Runs at any user logon
#------------------------------------------------------------#
# Variables
$run = (Get-Date).AddMinutes(2)
$ScheduleTime = (Get-Date).AddMinutes(15)
$triggers = @()
$taskname = 'Restart Intune Management Extension Service'
$taskdescription = 'Triggers a onetime restart of service IntuneManagementExtension. Forces device to sync with Intune Management '
$STSet = New-ScheduledTaskSettingsSet -AllowStartIfOnBatteries -ExecutionTimeLimit (New-TimeSpan -Minutes 5)
$action = New-ScheduledTaskAction -Execute 'powershell.exe' -Argument 'Restart-Service -Name IntuneManagementExtension'
# To trigger once after 10 mins of registering
$Trigger1 = New-ScheduledTaskTrigger -Once -At $ScheduleTime
# $Trigger1.Delay = 'PT10M'
$triggers += $Trigger1

#Register Task now
Register-ScheduledTask -Action $action -Trigger $triggers -TaskName $taskname -Description $taskdescription -User "SYSTEM" -RunLevel Highest -Settings $STSet
#Set task expiration after 60 mins and delete
$RegisteredTask = Get-ScheduledTask -TaskName "Restart Intune Management Extension Service"
$RegisteredTask.Triggers[0].EndBoundary = $run.AddMinutes(60).ToString('s')
$RegisteredTask.Settings.DeleteExpiredTaskAfter = 'PT0S'
$RegisteredTask | Set-ScheduledTask